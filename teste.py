#!/usr/bin/python
# -*- coding: utf-8 -*-

# Para rodar, chame o interpretador para esse arquivo Ex: python <teste.py>

import random
import unittest

suspects = [
    'Charles B. Abbage',
    'Donald Duck Knuth',
    'Ada L. Ovelace',
    'Alan T. Uring',
    'Ivar J. Acobson',
    'Ras Mus Ler Dorf',
    ]
location = [
    'Redmond',
    'Palo Alto',
    'San Francisco',
    'Tokio',
    'Restaurante no Fim do Universo',
    'São Paulo',
    'Cupertino',
    'Helsinki',
    'Maida Vale',
    'Toronto',
    ]
guns = [
    'Peixeira',
    'DynaTAC 8000X (o primeiro aparelho celular do mundo)',
    'Trezoitão',
    'Trebuchet',
    'Maça',
    'Gládio',
    ]

correct = [1, 2, 1]

def witness(killer, location, gun):
    infos = witnessInformation(killer, location, gun)
    return random.choice(infos)

def detective():
    correct = [random.choice(range(0, len(suspects))),
               random.choice(range(0, len(location))),
               random.choice(range(0, len(guns)))]

    attempts = [random.choice(range(0, len(suspects))),
                random.choice(range(0, len(location))),
                random.choice(range(0, len(guns)))]
    answer = witness(attempts[0], attempts[1], attempts[2])
    cont = 0
    while answer != 0:
        cont += 1
        if answer == 1:
            attempts = [random.choice(range(0, len(suspects))),
                        attempts[1], attempts[2]]
            answer = witness(attempts[0], attempts[1], attempts[2])
        if answer == 2:
            attempts = [attempts[0], random.choice(range(0,
                        len(location))), attempts[2]]
            answer = witness(attempts[0], attempts[1], attempts[2])
        if answer == 3:
            attempts = [attempts[0], attempts[1],
                        random.choice(range(0, len(guns)))]
            answer = witness(attempts[0], attempts[1], attempts[2])
    print 'Foram informados %d chutes' % cont
    print 'Assasino: %s' % suspects[attempts[0]]
    print 'Local: %s' % location[attempts[1]]
    print 'Arma: %s' % guns[attempts[2]]
    return attempts

def witnessInformation(killer, location, gun):
    returnArray = []
    if killer != correct[0]:
        returnArray.append(1)
    if location != correct[1]:
        returnArray.append(2)
    if gun != correct[2]:
        returnArray.append(3)
    if killer == correct[0] and location == correct[1] and gun \
        == correct[2]:
        returnArray.append(0)
    return returnArray


class WitnessTest(unittest.TestCase):

    def test_simple(self):
        self.assertEqual(0, witness(1, 2, 1))

    def test_correct(self):
        self.assertEqual(2, witness(1, 1, 1))
        self.assertEqual(1, witness(2, 2, 1))
        self.assertEqual(3, witness(1, 2, 2))

    def test_info_witness(self):
        self.assertEqual([0], witnessInformation(1, 2, 1))
        self.assertEqual([1], witnessInformation(2, 2, 1))
        self.assertEqual([2], witnessInformation(1, 1, 1))
        self.assertEqual([3], witnessInformation(1, 2, 2))

    def test_info_witness_two_wrong(self):
        self.assertEqual([1, 2], witnessInformation(2, 1, 1))
        self.assertEqual([2, 3], witnessInformation(1, 1, 2))
        self.assertEqual([1, 3], witnessInformation(2, 2, 2))

    def test_info_witness_tree_wrong(self):
        self.assertEqual([1, 2, 3], witnessInformation(0, 0, 0))

    def test_detective_response(self):
        self.assertEquals(correct, detective())


if __name__ == '__main__':
    unittest.main()
